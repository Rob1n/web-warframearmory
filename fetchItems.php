<?php
    session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>index</title>
    <link type="text/css" rel="stylesheet" href="css/scss/filterTabs.css" />
    <script src="js/fetchItems.js"></script>
    <?php
        require_once 'php/setupHead.php';
        require_once 'php/setupDB.php';
        $userID = 0;
        if(isset($_SESSION['userID'])){
            $userID = $_SESSION['userID'];
        }
    ?>
</head>

<body>
    
    <?php require_once 'php/setupNav.php'; ?>
    
    <div id="perspectiveOuter">
        <div id="perspectiveInner">
            
            <h1>BROWSER</h1>
            
            <div class="filterNew">
                <div class="filterLeft">
                    <div class="positioner">
                        <div class="menu">
                            <div class="menu_title">
                                Weapon slot:
                            </div>
                            <div class="menu_item">
                                <input class="toggle" name="menu_group" id="sneaky_toggle" type="radio" onchange="selectLeftFilter(1)" checked="checked">
                                <div class="expander">
                                    <label for="sneaky_toggle"><i class="menu_icon fa fa-bomb"></i> <span class="menu_text">All</span></label>
                                    <!--<label for="sneaky_toggle"><i class="menu_icon fa melee_icon"></i> <span class="menu_text">Primary</span></label>-->
                                </div>
                            </div>
                            <div class="menu_item">
                                <input class="toggle" name="menu_group" id="sneaky_toggle2" type="radio" onchange="selectLeftFilter(2)">
                                <div class="expander">
                                    <label for="sneaky_toggle2"><i class="menu_icon fa fa-bell"></i> <span class="menu_text">Primary</span></label>
                                </div>
                            </div>
                            <div class="menu_item">
                                <input class="toggle" name="menu_group" id="sneaky_toggle3" type="radio" onchange="selectLeftFilter(3)">
                                <div class="expander">
                                    <label for="sneaky_toggle3"><i class="menu_icon fa fa-child"></i> <span class="menu_text">Secondary</span></label>
                                </div>
                            </div>
                            <div class="menu_item">
                                <input class="toggle" name="menu_group" id="sneaky_toggle4" type="radio" onchange="selectLeftFilter(4)">
                                <div class="expander">
                                    <label for="sneaky_toggle4"><i class="menu_icon fa fa-bomb"></i> <span class="menu_text">Melee</span></label>
                                    <!--<label for="sneaky_toggle"><i class="menu_icon fa melee_icon"></i> <span class="menu_text">Primary</span></label>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="filterRight">
                    <div class="positioner">
                        <div class="menu">
                            <div class="menu_title">
                                Source:
                            </div>
                            <div class="menu_item">
                                <input class="toggle" name="menu_group2" id="sneaky_toggle11" type="radio" checked="checked" onchange="selectRightFilter(5)">
                                <div class="expander">
                                    <label for="sneaky_toggle11"><i class="menu_icon fa fa-bomb"></i> <span class="menu_text">All</span></label>
                                </div>
                            </div>
                            <div class="menu_item">
                                <input class="toggle" name="menu_group2" id="sneaky_toggle12" type="radio" onchange="selectRightFilter(1)">
                                <div class="expander">
                                    <label for="sneaky_toggle12"><i class="menu_icon fa fa-bell"></i> <span class="menu_text">Market</span></label>
                                </div>
                            </div>
                            <div class="menu_item">
                                <input class="toggle" name="menu_group2" id="sneaky_toggle13" type="radio" onchange="selectRightFilter(2)">
                                <div class="expander">
                                    <label for="sneaky_toggle13"><i class="menu_icon fa fa-child"></i> <span class="menu_text">Prime</span></label>
                                </div>
                            </div>
                            <div class="menu_item">
                                <input class="toggle" name="menu_group2" id="sneaky_toggle14" type="radio" onchange="selectRightFilter(3)">
                                <div class="expander">
                                    <label for="sneaky_toggle14"><i class="menu_icon fa fa-bell"></i> <span class="menu_text">Archwing</span></label>
                                </div>
                            </div>
                            <div class="menu_item">
                                <input class="toggle" name="menu_group2" id="sneaky_toggle15" type="radio" onchange="selectRightFilter(4)">
                                <div class="expander">
                                    <label for="sneaky_toggle15"><i class="menu_icon fa fa-bomb"></i> <span class="menu_text">Clan</span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="floatClear"></div>
            
            <section>
                <form class="fetch_orderBy">
                    <select name="users"> <!--onchange="getWeaponData(this.value)" add a new sorting parameter to javascript! -->
                        <option value="">Order by:</option>
                        <option value="3">Name</option>
                        <option value="3">Owned</option>
                        <option value="4">Mastered</option>
                    </select>
                </form>
                
                <div class="floatClear"></div>
            </section>

            <!-- ajax result location -->
            <div id="ajaxBrowserResult"></div>
    
        </div>
    </div>

    <?php
        require_once 'js/perspectiveEffect.php';
    ?>
    
    <!-- ajax initialization script -->
    <script type="text/javascript">
        function getWeaponData(str) {
            var xmlhttp;
            if (str == "") {
                document.getElementById("ajaxBrowserResult").innerHTML = "";
                return;
            } else {
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else {
                    // code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        document.getElementById("ajaxBrowserResult").innerHTML = xmlhttp.responseText;
                    }
                };
                xmlhttp.open("GET", "php/ajaxWeaponTable.php?q=" + str, true);
                xmlhttp.send();
            }
        }
    </script>
    
    <!-- applying filter to database query -->
    <!-- init - TODO so it loads user saved preference (last used) -->
    <script type="text/javascript">
        var currentSelectionL = 1;
        var currentSelectionR = 5;
        var currentSort = 2;
        selectLeftFilter(1);
        //selectRightFilter(5);

        function selectLeftFilter(selected){
            currentSelectionL = selected;
            getWeaponData(currentSelectionL + "" + currentSelectionR + "" + currentSort);
        }

        function selectRightFilter(selected){
            currentSelectionR = selected;
            getWeaponData(currentSelectionL + "" + currentSelectionR + "" + currentSort);
        }

        function selectSort(selected){
            currentSort = selected;
            getWeaponData(currentSelectionL + "" + currentSelectionR + "" + currentSort);
        }
    </script>
</body>