    <!-- perspective effect -->
    <script type="text/javascript">
        if (<?php if($_SESSION['PoVenabled']) echo "1"; else echo "0"; ?> == 1)
        {
            // POV ENABLED
            document.getElementById("perspectiveOuter").style.perspective = "500px";
            document.getElementById("perspectiveInner").style.transform = "rotateX(1deg) rotateY(-1deg)";
            document.getElementById("perspectiveInner").style.webkitTransform = "rotateX(1deg) rotateY(-1deg)";

            //mouse listener
            var myListener = function () {
                //get mouse position
                var e = window.event;
                var posX = e.clientX;
                var posY = e.clientY;

                //Algorithm.scale(valueIn, baseMin, baseMax, limitMin, limitMax);
                var scaledX = scale(posX, 0, screen.width, -1, +1);  // lijevo desno
                scaledX = Math.round(scaledX * 100) / 100;
                var scaledY = -scale(posY, 0, screen.height, -1, +1);  //gore dolje
                scaledY = Math.round(scaledY * 100) / 100;

                document.getElementById("perspectiveInner").style.transform = "rotateX("+scaledY+"deg) rotateY("+scaledX+"deg)";
                document.getElementById("perspectiveInner").style.webkitTransform = "rotateX("+scaledY+"deg) rotateY("+scaledX+"deg)";
            };

            function scale(valueIn, baseMin, baseMax, limitMin, limitMax) {
                return ((limitMax - limitMin) * (valueIn - baseMin) / (baseMax - baseMin)) + limitMin;
            }

            document.addEventListener('mousemove', myListener, false);  

        } else {
            document.getElementById("perspectiveOuter").style.perspective = "1000px"; 
        }
    </script>