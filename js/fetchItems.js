// confirmation button
var ghost = document.getElementById("confirmationGhost");
var changesTodo = [];

function checkChange(ID, name, type) {
    var index = changesTodo.indexOf(name + " " + type);
    if (index > -1) {
        changesTodo.splice(index, 1);
    } else {
        changesTodo.push(name + " " + type);
    }


    ghost.innerHTML = "";
    if (changesTodo.length > 0) {
        ghost.style.border = "1px solid red";
        ghost.style.borderRadius = "10px";
        ghost.style.display = "inline";
        ghost.innerHTML = '<input type="submit" name="applyGhost" value="Change selected items"></input>';
        for (var i=0; i < changesTodo.length; i++){
            ghost.innerHTML += "<br>" + changesTodo[i];
        }
    } else {
        ghost.style.display="none";
    }

}

