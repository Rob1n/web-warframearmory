<?php
    session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>index</title> <!-- browser not noticing newly added component weapons due to caching of list options -->
    <meta http-equiv="cache-control" content="no-cache"> <!-- tells browser not to cache -->
    <meta http-equiv="expires" content="0"> <!-- says that the cache expires 'now' -->
    <meta http-equiv="pragma" content="no-cache"> <!-- says not to use cached stuff, if there is any -->
    <?php
        require_once 'php/setupDB.php';
        require_once 'php/setupHead.php';
    ?>
</head>

<body>
    
    <?php require_once 'php/setupNav.php'; ?>
    
    <?php
        if (!isset($_SESSION['userID']) || ($_SESSION['accessLevel'] < 3)){
            echo "<br><span style='color:red;'>Insufficient user privileges to access this page.</span>";
        } else { 
    ?>
    
    <section>
        <form name="addItem" method="POST" action="addItemConfirmation.php">
            <fieldset>
                
                Item name: 
                <input type="text" name="itemName"><br>
                
                Item source: 
                <select name="source">
                    <?php
                        $conn = new mysqli($dbadd, $dbuser, $dbpass, $dbname);
                        if ($conn->connect_error) {
                            die("Connection failed: " . $conn->connect_error);
                        }

                        $sql = "SELECT * FROM sources";
                        $result = $conn->query($sql);
                        if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {
                                echo "<option value=".$row['ID'].">".$row['name']."</option>";
                            }
                        }

                    ?>
                </select><br>
                
                Item slot: 
                    <select name="itemSlot">
                        <?php
                            $sql = "SELECT * FROM slots";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {
                                    echo "<option value=".$row['ID'].">".$row['name']."</option>";
                                }
                            }

                        ?>
                    </select><br>
                
                Component #1: 
                    <select name="component">
                        <?php
                            $sql = "SELECT * FROM weapons ORDER BY name ASC";
                            $result = $conn->query($sql);
                            echo "<option value='none'></option>";
                            if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {
                                    echo "<option value=".$row['ID'].">".$row['name']."</option>";
                                }
                            }

                        ?>
                    </select>
                Quantity:
                    <select name="cQ">
                        <option value="1">1</option>
                        <option value="2">2</option>                
                    </select>
                <br>
                
                Component #2: 
                    <select name="component2">
                        <?php
                            $sql = "SELECT * FROM weapons ORDER BY name ASC";
                            $result = $conn->query($sql);
                            echo "<option value='none'></option>";
                            if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {
                                    echo "<option value=".$row['ID'].">".$row['name']."</option>";
                                }
                            }

                            $conn->close();

                        ?>
                    </select>
                Quantity:
                    <select name="c2Q">
                        <option value="1">1</option>
                        <option value="2">2</option>                
                    </select>
                <br>
                
                <input type="submit" value="Add item">
                
            </fieldset>
        </form>
        
    </section>

    <?php } ?>
    
</body>