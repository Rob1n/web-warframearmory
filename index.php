<?php
    session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>index</title>
    <?php require_once 'php/setupHead.php'; ?>
</head>

<body>
    <?php require_once 'php/setupNav.php'; ?>

    <section>
        <p>Welcome! We're almost done, the site is (pretty much) functional but still lacking weapon data. Registrations are open and bugtesting is welcomed.</p>
        <p>If you have any feature requests or bugs to report just whine on Mumble until I get there, I don't have a contact address yet. Also, try not to break anything.</p>
        
        <p><span style='font-weight:bold;font-size:20px;'>About the tool:</span></p>
        <p>This site will enable you to store the information about all your weapons in Warframe - specifically, which ones you own already and which ones you mastered (leveled to max level). You are also able to easily see whether these items are used as components in any other crafting recipes. </p>
        
        <p>The primary goal of this tools is to help it's user track which items should you keep instead of selling. Never again sell an item that you'll need later! The secondary goal is to recommend items for users to build next based on their current weapon profiles (ETA unknown).</p>
    </section>
    
</body>