<?php
    session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>index</title>
    <?php
        require_once 'php/setupDB.php';
        require_once 'php/setupHead.php';
    ?>
</head>

<body>
    
    <?php require_once 'php/setupNav.php'; ?>
    
    <?php

        if (!isset($_SESSION['userID']) || ($_SESSION['accessLevel'] < 3)){
            echo "<br><span style='color:red;'>Insufficient user privileges to access this page.</span>";
        } else { 
            $newItemName = $_POST['itemName'];
            $sourceID = $_POST['source'];
            $slotID = $_POST['itemSlot'];
            $userAdding = $_SESSION['userID'];

            $conn = new mysqli($dbadd, $dbuser, $dbpass, $dbname);
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            //check if item already exists
            $stmt = $conn->prepare("SELECT COUNT(1) FROM weapons WHERE name = ?;");
            $stmt->bind_param("s", $newItemName);
            $stmt->bind_result($duplicateResult);
            $stmt->execute();
            $stmt->fetch();
            $stmt->close();
            if ($duplicateResult > 0){
                echo "<br>Item with that name already exists in the database.";
            } else {
                $stmt = $conn->prepare("INSERT INTO weapons (name, source, slot, addedByUser) VALUES (?, ?, ?, ?);");
                $stmt->bind_param("siii", $newItemName, $sourceID, $slotID, $userAdding);
                $stmt->execute();
                $stmt->close();

                #component
                if (($_POST['component'] != "none") || ($_POST['component2'] != "none")) {

                    $stmt = $conn->prepare("SELECT ID FROM weapons WHERE name = ? AND source = ? AND slot = ?;");
                    $stmt->bind_param("sii", $newItemName, $sourceID, $slotID);
                    $stmt->bind_result($newItemID);
                    $stmt->execute();
                    $stmt->fetch();
                    $stmt->close();         

                    $stmt = $conn->prepare("INSERT INTO components (itemID, usedIn, quantity) VALUES (?, ?, ?);");
                    $stmt->bind_param("iii", $componentID, $newItemID, $quantity);
                    if ($_POST['component'] != "none"){
                        $componentID = $_POST['component'];
                        $quantity = $_POST['cQ'];
                        $stmt->execute();
                    }
                    //echo "<br>ID: ".$itemID." usedIn: ".$usedIn." quantity: ".$quantity;
                    if ($_POST['component2'] != "none"){
                        $componentID = $_POST['component2'];
                        $quantity = $_POST['c2Q'];
                        $stmt->execute();
                    }

                    $stmt->close();
                    $conn->close();
                    echo "<br>item added successfully";
                }
            }
        }
    ?>

    
</body>