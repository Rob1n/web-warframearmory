<?php
    session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>index</title>
    <?php
        require_once 'php/setupDB.php';
        require_once 'php/setupHead.php';
    ?>
</head>

<?php
    $conn = new mysqli($dbadd, $dbuser, $dbpass, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    if (isset($_SESSION['userID'])){
        session_unset();
        session_destroy();
    }

    if (isset($_POST['loginSubmit'])){
        $username = $_POST['loginUsername'];
        $password = $_POST['loginPassword'];
        $password = md5($password);

        $stmt = $conn->prepare("SELECT ID, username, accesslevel FROM users WHERE username = ? AND password = ? ;");            
        $stmt->bind_param("ss", $username, $password);
        $stmt->bind_result($rUserID, $rUsername, $rAccessLevel);
        $stmt->execute();
        $stmt->fetch();
        $stmt->close();
        if ($rUserID != ""){
            echo "<br>success! User ID: ".$rUserID." logged in! Access level: ".$rAccessLevel."<br>";   
            //setup user session
            $_SESSION['userID'] = $rUserID;
            $_SESSION['username'] = $rUsername;
            $_SESSION['accessLevel'] = $rAccessLevel;
            //redirectino to home page
            header("Location: index.php");
            die();
        } else {
            echo "<br><p style='color:red;'>Incorrect user credentials.</p>";
        }
    }

    if (isset($_POST['registerSubmit'])){
        $username = $_POST['registerUsername'];
        $mail = $_POST['registerMail'];
        $password = $_POST['password'];
        $password = md5($password);
        $access = "4";
        $currentTime = date("Y-m-d H:i:s");

        //check if username already exists
        $stmt = $conn->prepare("SELECT COUNT(1) FROM users WHERE username = ? ;");
        $stmt->bind_param("s", $username);
        $stmt->bind_result($result);
        $stmt->execute();
        $stmt->fetch();
        $stmt->close();
        if ($result > 0){
            echo "<br><p style='color:red;'>Username already in use.</p>";
        } else {
            $stmt = $conn->prepare("INSERT INTO users (username, password, mail, accesslevel, timeRegistered) VALUES (?, ?, ?, ?, ?);");
            $stmt->bind_param("sssis", $username, $password, $mail, $access, $currentTime);
            $stmt->execute();
            $stmt->close();
            echo "<br><p>Congratulations, registration successfully completed! Please log in with your new user credentials.</p><br>";
        }
    }

    $conn->close();
?>
    
<body>
    <?php require_once 'php/setupNav.php'; ?>
    
    <section id="loginForm">
        <form name="loginForm" method="POST" action="<?=$_SERVER['PHP_SELF']?>">
            <p style="font-weight: bold;">Login for existing users</p>
            <fieldset>
                <p>
                    <label for="loginUsername">Username: </label>
                    <input type="text" name="loginUsername">
                </p>
                <p>
                    <label for="loginPassword">Password: </label>
                    <input type="password" name="loginPassword">
                </p>
                
                <input type="submit" name="loginSubmit" value="Login">
            </fieldset>
        </form>
        
        <form name="registerForm" method="POST" action="<?=$_SERVER['PHP_SELF']?>">
            <p style="font-weight: bold;">New user registration</p>
            <p>Entering your e-mail address is not mandatory. However if you don't, you will be unable you to reset your password if you ever lose it.</p>
            <br>
            <fieldset>
                <p>
                    <label for="registerUsername">Username: </label>
                    <input type="text" name="registerUsername" id="registerUsername">
                    <span id="userWarning" style="color:red;"></span>
                </p>
                
                <p>
                    <label for="mail">E-mail address (optional):</label>
                    <input type="text" name="registerMail" id="registerMail">
                    <span id="mailWarning" style="color:red;"></span>
                </p>
                
                <p>
                    <label for="password">Password: </label>
                    <input type="password" name="password" id="password1">
                    <span id="passWarning" style="color:red;"></span>
                </p>
                
                <p>
                    <label for="confirmPassword">Confirm password:</label>
                    <input type="password" name="confirmPassword" id="password2">
                    <span id="pass2Warning" style="color:red;"></span>
                </p>
                

                
                <input type="submit" name="registerSubmit" value="Register" onclick="checkRegistration();">
            </fieldset>
        </form>
        
    </section>    
    
    <script type="text/javascript">
        function checkRegistration(){
            var username = document.getElementById("registerUsername");
            var mail = document.getElementById("registerMail");
            var password1 = document.getElementById("password1");
            var password2 = document.getElementById("password2");
            
            var userWarning = document.getElementById("userWarning");
            var mailWarning = document.getElementById("mailWarning");
            var passWarning = document.getElementById("passWarning");
            var pass2Warning = document.getElementById("pass2Warning");
            
            var allowPass = true;
            
            if (username.value.length < 3){
                userWarning.innerHTML="Username is too short (has to be between 3 and 20 characters)";
                allowPass = false;
            } else if (username.value.length > 20) {
                userWarning.innerHTML="Username is too long (has to be between 3 and 20 characters)";
                allowPass = false;
            } else {
                userWarning.innerHTML="";
            }
            
            if (password1.value.length < 5 ){
                passWarning.innerHTML="Password is too short (has to be between 5 and 20 characters)"; 
                allowPass = false;
            } else if (password1.value.length > 20){
                passWarning.innerHTML="Password is too long (has to be between 5 and 20 characters)"; 
                allowPass = false;
            } else {
                passWarning.innerHTML="";
            }
            
            if (password1.value != password2.value){
                pass2Warning.innerHTML="Passwords do not match"; 
                allowPass = false;
            } else {
                pass2Warning.innerHTML="";
            }
            
            if (allowPass == false){
                event.preventDefault();
                event.stopPropagation();
            }
        }
    </script>    
</body>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    