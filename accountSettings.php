<?php
    session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>index</title>
    <?php
        require_once 'php/setupDB.php';
        require_once 'php/setupHead.php';
    ?>
</head>

<body>
    
    <?php require_once 'php/setupNav.php'; ?>
    
    <?php
        if (!isset($_SESSION['userID'])) {
            echo "<br><span style='color:red;'>You need to be logged in to access this page.</span>";
        } else {
            
            if (isset($_POST['applySettingsSubmit'])){
                if (isset($_POST['enablePoV'])){
                    $_SESSION['PoVenabled'] = true;
                } else {
                    $_SESSION['PoVenabled'] = false;   
                }
                echo "<br>New settings applied.<br>";
            } else {
            // continue onwards            
    ?>
    
    <section>
        
        <form class="fetch_disablePoV" method="POST" action="<?=$_SERVER['PHP_SELF']?>">
            <label for="enablePoV">Enable PoV effect: </label>
            <input type="checkbox" name="enablePoV" id="enablePoV" value="enable"
                   onchange="togglePoV.call(this)" <?php if (isset($_SESSION['PoVenabled']) && ($_SESSION['PoVenabled'] == true)) echo "checked='checked'"; ?>  >
            
            
            <br><br>
            <input type="submit" name="applySettingsSubmit" value="Apply">
        </form>
        
        
        
        
    </section>
    <?php 
            }
        }

    ?>
    
</body>