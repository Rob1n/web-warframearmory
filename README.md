# Warframe completionist tool

A tool to show you missing items for your mastery rank. Easily tag owned and mastered (max rank) items and check whether they are used in any other recipes. 

It's personal testbed for learning Ajax, managing DBs and pulling all data from my db, hosting the application on a personal server, http sessions, manually creating a safe, encrypted, user registration and login services as well as having custom roles,... 

Not feature complete but the core is working fine. Will probably be re-done with RoR eventually, for safety and optimization purposes, using proper MVC structure.

A demo can be found at : http://kaldocorp.ddns.net/warframe.git/
