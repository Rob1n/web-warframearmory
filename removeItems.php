<?php
    session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>index</title>
    <?php
        require_once 'php/setupDB.php';
        require_once 'php/setupHead.php';
    ?>
</head>
    
<body>
    
    <?php require_once 'php/setupNav.php'; ?>
    
    <?php
        if (!isset($_SESSION['userID']) || ($_SESSION['accessLevel'] < 4)){
            echo "<br><span style='color:red;'>Insufficient user privileges to access this page.</span>";
        } else { 
    ?>
    
    <section style="padding-bottom:20px;">
        <form name="search" method="POST" action="removeItems.php">
            <fieldset>
                <label for="itemName">Enter item name: </label>
                <?php
                    if (isset($_POST['itemName']))
                        echo '<input type="text" name="itemName" value="'.$_POST['itemName'].'">';
                    else
                        echo '<input type="text" name="itemName">';
                ?>
                <input type="submit" name="search" value="search">
            </fieldset>
        </form>
        <p>WARNING - deleting items used in production of other items will also delete all these successive items, as well as all crafting recipes that use these items!</p>
    </section>

    <section>
        <form name="searchOverview" method="POST">                
                <?php
            
                    $conn = new mysqli($dbadd, $dbuser, $dbpass, $dbname);
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    }


                    // TODO transfer to ajax
                    if (isset($_POST['search'])){
                        echo '<table id="browserTable">
                                <tr>
                                    <th>Item name</th>
                                    <th>Source</th>
                                    <th>Slot</th>
                                    <th>Used in</th>
                                    <th>Delete</th>
                                </tr>';
                        $target = $_POST['itemName'];
                        $stmt = $conn->prepare("SELECT id, name, source, slot FROM weapons WHERE name LIKE ? ;");
                        if (!$stmt) die("isOwned stmt errormessage: ".$GLOBALS['conn']->error);

                        $target = "%".$target."%";
                        $stmt->bind_param("s", $target);
                        $stmt->bind_result($id, $name, $source, $slot);
                        $stmt->execute();
                        
                        $weaponResult = $stmt->get_result();
                        $stmt->close();
                        while ($row = $weaponResult->fetch_array(MYSQLI_ASSOC)){
                            
                            echo "<tr>";
                            echo "<td>".$row['name']."</td>";
                            echo "<td>".$row['source']."</td>";
                            echo "<td>".$row['slot']."</td>";

                            #weapons produced by name
                            $stmt = $conn->prepare("SELECT s.name sName, r.name rName, components.quantity
                                        FROM weapons s, weapons r
                                        JOIN components ON r.ID = components.usedIn
                                        WHERE components.itemID = s.ID
                                        AND s.ID = ? ;");
                            $stmt->bind_param("i", $row['id']);
                            $stmt->bind_result($sName, $rName, $quantity);
                            $stmt->execute();
                            
                            echo "<td>";
                            while ($stmt->fetch()){
                                echo $rName;
                                if ($quantity > 1)
                                    echo " (requires ".$quantity.")";
                                echo "\n";
                            }                            
                            echo "</td>";

                            #delete checkbox
                            echo "<td><input type='checkbox' value='cb".$row['id']."'></td>";

                            echo "</tr>";
                        }

                        $conn->close();
                        echo "<table>";
                        echo '<p><input type="submit" value="Delete selected"></p>';
                    }
                ?>

        </form>    
    </section>
    
    <?php } ?>
    
</body>