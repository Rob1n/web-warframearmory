<?php ?>
    <nav>
        <ul class="mainNavUl">
            <li><a href="index.php">Index</a></li>
            <li><a href="fetchItems.php">Browse</a></li>
            <?php
                if(isset($_SESSION['userID'])){
                    if ($_SESSION['accessLevel'] > 2){
                        echo "<li><a href='addItems.php'>Add new items</a></li>";
                        if ($_SESSION['accessLevel'] > 3){
                            echo "<li><a href='removeItems.php'>Remove items</a></li>";
                        }
                    } 
                    echo "<li><a href='php/logout.php'>Logout (".$_SESSION['username'].")</a></li>";
                    echo "<li><a href='accountSettings.php'>Account settings</a></li>";
                } else {
                    echo "<li><a href='login.php'>Login / Register</a></li>";
                }
            ?>  
            <li>About</li>
        </ul>
    </nav>
<?php ?>