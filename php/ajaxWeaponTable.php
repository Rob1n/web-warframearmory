<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    
<head>    
    <title>index</title>
    <?php
        $myfile = fopen("../../dbinfo.txt", "r") or die("Unable to open file!");
        $dbadd = rtrim(fgets($myfile));
        $dbuser = rtrim(fgets($myfile));
        $dbpass = rtrim(fgets($myfile));
        $dbname = rtrim(fgets($myfile));
        fclose($myfile);

        require_once 'setupHead.php';
        $userID = 0;
        if(isset($_SESSION['userID'])){
            $userID = $_SESSION['userID'];
        }
    ?>
</head>
<body>
    <section class="itemOverview">
        <form name="itemOverview" method="POST">
            
        <?php
            $q = $_GET['q'];
            $l = $q[0];
            $r = $q[1];

            $conn = new mysqli($dbadd, $dbuser, $dbpass, $dbname);
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            echo '<table id="browserTable">
                    <tr>
                        <th>Name</th>
                        <th>Owned</th>
                        <th>Mastered</th>
                        <th>Used in crafting</th>
                    </tr>';

            # INITIAL FETCH
            $stmt;
            if ($l == 1) {
                if ($r == 5){
                    $stmt = $conn->prepare("SELECT ID, name FROM weapons ORDER BY 2 ASC");
                } else {
                    $stmt = $conn->prepare("SELECT ID, name FROM weapons WHERE source = ? ORDER BY 2 ASC");
                    $stmt->bind_param("i", $r);
                }
            }
            else {
                if ($r == 5){
                    $stmt = $conn->prepare("SELECT ID, name FROM weapons WHERE slot = ? ORDER BY 2 ASC; ");
                    $stmt->bind_param("i", $l);
                } else {
                    $stmt = $conn->prepare("SELECT ID, name FROM weapons WHERE slot = ? AND source = ? ORDER BY 2 ASC; ");
                    $stmt->bind_param("ii", $l, $r);
                }
            }

            $stmt->bind_result($weaponID, $weaponName);
            $stmt->execute();
            $weaponResult = $stmt->get_result();
            $stmt->close();

            while ($data = $weaponResult->fetch_array(MYSQLI_ASSOC)){
                echo "<tr>";

                # TABLE NAME
                echo "<td><span>" . $data['name'] . "</span></td>";

                # TABLE OWNED
                if (isOwned($data['ID'], $userID)){
                    echo "<td style='border: 2px solid green; border-collapse: collapse; background-color:#8ce690;'>";
                    echo "<label class='omWrapper'>Yes";
                    echo "<input type='checkbox' name='owned[]' id='o".$data['ID']."' value='o".$data['ID']."'
                            onclick='checkChange(".$data['ID'].", \"".$data['name']."\",\"owned\")'>";
                    echo "</label>";
                } else {
                    echo "<td><label class='omWrapper'>No";
                    echo "<input type='checkbox' name='owned[]' id='o".$data['ID']."' value='o".$data['ID']."'
                        onclick='checkChange(".$data['ID'].", \"".$data['name']."\",\"owned\")'>";
                    echo "</label>";
                }
                echo "</td>";
                
                # TABLE MASTERED
                if (isMastered($data['ID'], $userID)){
                    echo "<td style='border: 2px solid green; border-collapse: collapse; background-color:#8ce690;'>";
                    echo "<label class='omWrapper'>Yes";
                    echo "<input type='checkbox' name='mastered[]' id='o".$data['ID']."' value='o".$data['ID']."'
                            onclick='checkChange(".$data['ID'].", \"".$data['name']."\",\"mastered\")'>";
                    echo "</label>";
                } else {
                    echo "<td><label class='omWrapper'>No";
                    echo "<input type='checkbox' name='mastered[]' id='o".$data['ID']."' value='o".$data['ID']."'
                        onclick='checkChange(".$data['ID'].", \"".$data['name']."\",\"mastered\")'>";
                    echo "</label>";
                }
                echo "</td>";
                
                # WEAPONS PRODUCED by name
                $stmt2 = $conn->prepare('SELECT s.name sName, r.name rName, components.quantity
                                FROM weapons s, weapons r
                                JOIN components ON r.ID = components.usedIn
                                WHERE components.itemID = s.ID
                                AND s.ID = ? ;');
                $stmt2->bind_param("i", $weaponID);
                $stmt2->bind_result($sourceName, $resultName, $quantity);
                $stmt2->execute();
                echo "<td>";
                while ($stmt2->fetch()){
                    echo $resultName;
                    if ($quantity > 1)
                        echo " (requires ".$quantity.")";
                    echo "<br>";
                }
                echo "</td>";
                echo "</tr>";
                $stmt2->close();
            }

            # FUNCTIONS
            function isOwned($weaponID, $userID){
                $stmt = $GLOBALS['conn']->prepare("SELECT COUNT(1) FROM userowned WHERE itemID = ? AND userID = ?;");
                if (!$stmt) die("isOwned stmt errormessage: ".$GLOBALS['conn']->error);
                $stmt->bind_param("ii", $weaponID, $userID);
                $stmt->bind_result($result);
                $stmt->execute();
                
                $intresult = 0;
                while ($stmt->fetch()){
                    $intresult = $result;    
                }
                $stmt->close();
                if ($intresult > 0)
                    return 1;
                return 0;
            }

            function isMastered($weaponID, $userID){
                $stmt = $GLOBALS['conn']->prepare("SELECT COUNT(1) FROM usermastered WHERE itemID = ? AND userID = ?;");
                $stmt->bind_param("ii", $weaponID, $userID);
                $stmt->bind_result($result);
                $stmt->execute();
                
                $intresult = 0;
                while ($stmt->fetch()){
                    $intresult = $result;    
                }
                $stmt->close();
                if ($intresult > 0)
                    return 1;
                return 0; 
            }

        ?>
        </table>
        <span id="confirmationGhost" ><input type="submit" name="applyGhost" value="Change selected items"></input></span>

        </form>
    </section>
    
    
</body>
</html>